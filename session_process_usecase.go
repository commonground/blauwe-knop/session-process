// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package sessionprocess

import (
	"crypto"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"time"

	"crypto/rsa"

	"github.com/coreos/rkt/tests/testutils/logger"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
	"go.uber.org/zap"
	"golang.org/x/crypto/ssh"
)

var ErrPublicKeyIsInvalid = errors.New("public key is invalid")
var ErrSignedMessageInvalid = errors.New("signed message invalid")
var ErrSignatureInvalid = errors.New("signature not signed with the correct private key")

type Challenge struct {
	PublicKey string
	Random    string
	Date      time.Time
}

type SessionRandomGenerator interface {
	GenerateSessionToken() SessionToken
	GenerateChallengeRandom() string
}

type SessionRegisterRepository interface {
	CreateSession(publicKeyPem string) (*SessionToken, error)
	CreateChallenge(publicKeyFingerprint, publicKeyPem string) (*Challenge, error)
	GetChallenge(publicKeyFingerPrint string) (*Challenge, error)
	healthcheck.Checker
}

type SessionProcessUseCase struct {
	sessionRegisterRepository SessionRegisterRepository
	logger                    *zap.Logger
}

func NewSessionProcessUseCase(logger *zap.Logger, sessionRegisterRepository SessionRegisterRepository) *SessionProcessUseCase {
	return &SessionProcessUseCase{
		sessionRegisterRepository: sessionRegisterRepository,
		logger:                    logger,
	}
}

func (a *SessionProcessUseCase) StartChallenge(publicKeyPEM string) (*Challenge, error) {
	_, publicKeyFingerPrint, err := parsePublicKey(publicKeyPEM)
	if err != nil {
		logger.Error("unable to parse public key", zap.Error(err))
		return nil, ErrPublicKeyIsInvalid
	}
	a.logger.Debug("public key finger print", zap.String("fingerprint", publicKeyFingerPrint))

	challenge, err := a.sessionRegisterRepository.CreateChallenge(publicKeyFingerPrint, publicKeyPEM)
	if err != nil {
		logger.Error("unable to create challenge", zap.Error(err))
		return nil, err
	}
	a.logger.Debug("challenge created", zap.String("random", challenge.Random), zap.Time("date", challenge.Date))

	return challenge, nil
}

func (a *SessionProcessUseCase) CompleteChallenge(publicKeyPEM, signedMessage string) (*SessionToken, error) {
	a.logger.Debug("input parameters", zap.String("publicKeyPEM", publicKeyPEM), zap.String("signedMessage", signedMessage))

	publicKey, publicKeyFingerPrint, err := parsePublicKey(publicKeyPEM)
	if err != nil {
		logger.Error("unable to parse public key", zap.Error(err))
		return nil, ErrPublicKeyIsInvalid
	}
	a.logger.Debug("public key finger print", zap.String("fingerprint", publicKeyFingerPrint))

	challenge, err := a.sessionRegisterRepository.GetChallenge(publicKeyFingerPrint)
	if err != nil {
		logger.Error("unable to retrieve challenge", zap.Error(err))
		return nil, err
	}
	a.logger.Debug("challenge retrieved", zap.String("random", challenge.Random), zap.Time("date", challenge.Date))

	expectedSignedMessageContent := fmt.Sprintf("%s%d", challenge.Random, challenge.Date.UTC().Unix())
	a.logger.Debug("expected signed message content", zap.String("message", expectedSignedMessageContent))

	digest, err := createDigest(expectedSignedMessageContent)
	if err != nil {
		logger.Error("unable to create digest", zap.Error(err))
		return nil, err
	}

	decodedBytes, err := base64.StdEncoding.DecodeString(signedMessage)
	if err != nil {
		logger.Error("unable to decode signed message", zap.Error(err))
		return nil, ErrSignedMessageInvalid
	}

	err = rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, digest, decodedBytes)
	if err != nil {
		logger.Error("unable to verify signed message", zap.Error(err))
		return nil, ErrSignatureInvalid
	}
	a.logger.Debug("signed message verified")

	sessionToken, err := a.sessionRegisterRepository.CreateSession(publicKeyPEM)

	a.logger.Debug("CreateSession:", zap.String("sessionToken", string(*sessionToken)))
	a.logger.Debug("err: %s", zap.Error(err))
	return sessionToken, err
}

func parsePublicKey(publicKeyPEM string) (publicKey *rsa.PublicKey, fingerPrint string, err error) {
	publicKey, err = parsePublicKeyPEM(publicKeyPEM)
	if err != nil {
		return nil, "", err
	}

	fingerPrint, err = getCertificateFingerPrint(publicKey)
	if err != nil {
		return nil, "", err
	}

	return publicKey, fingerPrint, err
}

func parsePublicKeyPEM(publicKeyPEM string) (*rsa.PublicKey, error) {
	blockPublic, _ := pem.Decode([]byte(publicKeyPEM))
	if blockPublic == nil {
		return nil, fmt.Errorf("cannot parse pem")
	}

	key, err := x509.ParsePKCS1PublicKey(blockPublic.Bytes)
	return key, err
}

func getCertificateFingerPrint(publicKey *rsa.PublicKey) (string, error) {
	sshPublicKey, err := ssh.NewPublicKey(publicKey)
	if err != nil {
		return "", err
	}

	return ssh.FingerprintSHA256(sshPublicKey), nil
}

func createDigest(message string) ([]byte, error) {
	hasher := sha256.New()
	_, err := hasher.Write([]byte(message))
	if err != nil {
		return nil, err
	}
	return hasher.Sum(nil), nil
}

func (a *SessionProcessUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.sessionRegisterRepository,
	}
}
