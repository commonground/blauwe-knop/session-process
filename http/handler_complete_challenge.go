// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/render"

	"sessionprocess"
)

type completeChallengePayload struct {
	PublicKeyPem    string `json:"publicKeyPem"`
	SignedChallenge string `json:"signedChallenge"`
}

type completeChallengeResponse struct {
	SessionToken string `json:"sessionToken"`
}

func handlerCompleteChallenge(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionProcessUseCaseKey).(*sessionprocess.SessionProcessUseCase)

	requestPayload := &completeChallengePayload{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyPem == "" {
		log.Printf("a public key is required")
		http.Error(w, "a public key is required", http.StatusBadRequest)
		return
	}

	if requestPayload.SignedChallenge == "" {
		log.Printf("a signed message is required")
		http.Error(w, "a signed message is required", http.StatusBadRequest)
		return
	}

	sessionToken, err := sessionUseCase.CompleteChallenge(requestPayload.PublicKeyPem, requestPayload.SignedChallenge)
	if err != nil {
		log.Printf("error getting challenge: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	responsePayload := completeChallengeResponse{
		SessionToken: string(*sessionToken),
	}

	err = json.NewEncoder(w).Encode(responsePayload)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
