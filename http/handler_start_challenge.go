// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"log"
	"net/http"

	"sessionprocess"

	"github.com/go-chi/render"
)

type startChallengePayload struct {
	PublicKeyPEM string `json:"publicKeyPem"`
}

type startChallengeResponse struct {
	Date   int64  `json:"date"`
	Random string `json:"random"`
}

func handlerStartChallenge(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionProcessUseCaseKey).(*sessionprocess.SessionProcessUseCase)

	requestPayload := &startChallengePayload{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyPEM == "" {
		log.Printf("a public key is required")
		http.Error(w, "a public key is required", http.StatusBadRequest)
		return
	}

	challenge, err := sessionUseCase.StartChallenge(requestPayload.PublicKeyPEM)
	if err != nil {
		log.Printf("error creating challenge: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(mapChallengeToStartChallengeResponse(challenge))
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func mapChallengeToStartChallengeResponse(challenge *sessionprocess.Challenge) *startChallengeResponse {
	return &startChallengeResponse{
		Date:   challenge.Date.UTC().Unix(),
		Random: challenge.Random,
	}
}
