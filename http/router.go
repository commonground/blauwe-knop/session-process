// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"sessionprocess"
)

type key int

const (
	sessionProcessUseCaseKey key = iota
)

func NewRouter(sessionProcessUseCase *sessionprocess.SessionProcessUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/sessions", func(r chi.Router) {
		r.Post("/startchallenge", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionProcessUseCaseKey, sessionProcessUseCase)
			handlerStartChallenge(w, r.WithContext(ctx))
		})
		r.Post("/completechallenge", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionProcessUseCaseKey, sessionProcessUseCase)
			handlerCompleteChallenge(w, r.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("session-process", sessionProcessUseCase.GetHealthChecks())
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
