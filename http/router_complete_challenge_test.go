// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"sessionprocess"
	http_infra "sessionprocess/http"
	"sessionprocess/mock"
)

const publicKeyPEM = `-----BEGIN PUBLIC KEY-----
MIICCgKCAgEAupwib7oqzjY2G6TP8Tw4S4Wi63++wV3J/pqpqG6k7feGwE60HUaSNUSjHOV8ftbqkILjhcnWQZQIOdGqyo+UQKlKa8lRjgZSDlUrVzUUcNU5bBT1jeAu0UkV9GuqMD67JMfswL0LjRi9Rbs1fqrfK3Z0oRs0cqdnn3I2aClaDfuPhQqL9ZPZ9Ho3wrnSynCIWq7Sj8FO+hP7mcLpUXqu9An7VssxDN8pNc0ltz2I4IbfVXVb6hsDFsSEDtwe3pVA0bPhKbCgCE269JsuO2uh+P1Dtrd/AACn1vFImkTtllo27/1pIi7Zj6teFT9hKeuBerKG58kXipVQVD6FxMdnzhym0eKC1iFyrQOrv37Q9eAOaS/U40PgVRUMJDkNVfqcNwkYLsefrtJY+DEUACStMccn673bqjHhGQh12hYTZDHqGX8ikrmKHUw0RRiAFEda3Muh1kfmBRFiFEvXxhFmm6F7zZjPkaS0wO7dNwJbcDAW/EgTA0eNDsR4yv4NGp9bsMb3hdNnPTA3U/c+wkVbu0zaSAbJ3HOMBpVj2NXMK7XXSJBqFhbeaX99plb3CrfrfLfTOeG2IgrMFN+C2MY7DIh4XVXDKLp7U+VjgcK5bOUhBsvkDYVOgMTPb05+rsLPZhiW7IRk6JZV4jIDhJA4hnqlvaXjFsMP14JtcU1JBLUCAwEAAQ==
-----END PUBLIC KEY-----`
const publicKeyFingerPrint = `SHA256:CebvMD2TyTtWlbOg5zQspu49sSgGZzN1ylH3eDEoMKc`

func Test_CreateRouter_CompleteChallenge(t *testing.T) {
	logger := zap.NewNop()

	mockDate := time.Unix(1598975460, 0)

	type fields struct {
		sessionRegisterRepository sessionprocess.SessionRegisterRepository
	}
	type args struct {
		publicKeyPEM    string
		signedChallenge string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				sessionRegisterRepository: func() sessionprocess.SessionRegisterRepository {
					repo := generateSessionProcessRepository(t)
					repo.EXPECT().GetChallenge(gomock.Any()).AnyTimes().Return(&sessionprocess.Challenge{
						PublicKey: "",
						Date:      mockDate,
						Random:    "mock-random",
					}, nil)
					sessionToken := sessionprocess.SessionToken("mock-session-token")
					repo.EXPECT().CreateSession(publicKeyPEM).AnyTimes().Return(&sessionToken, nil)
					return repo
				}(),
			},
			args{
				publicKeyPEM:    publicKeyPEM,
				signedChallenge: "hqi94eaJi/bTwsP4+aliX/IEMb0QryK04zwU9x2GUsjaJ441d5a0Rvp3xEFo3QWSjiFgEwXOCMDaKo5QuxE6aU1qM2NGRDRzFQeqPohjxDHI/DfoMmO7TCg+jtdS7NGYxCREALWzVP20bDG1uZPNYW3S3G6VlL8J/p70ONS3VnaZkgZXu5rP8E1NygibEijlVFg5L9oIOdZ/XUVZlUtAdJG1/sz9QFXF61iOfh5g5LD6pz0pMTPaEVTzGhz8PKZI5PKXt+wAPTPJhLUqmKncJpUfz3g5CaABRVoUWP1WBTr9h9PPs2G/wgaR7scLh4Sqn0C6DhH+WusSND9o/hjtlruCnVDop0+b4+gW0PRRXq8W53ej9MD+4zReBo/Zy+WiCK8m8yIdQ8PQpL8nC4CKzCsue+AvRGtzPOFFa3YbpVHAZvnVD8HlCP14dw9ZpkAQrLocG/8QgoWJUj+T1AWxGEGsaCSkkAhkCI1zb1B+cMcmWjo3fbwvn2ZhkICRwhs/E9BSsKhIz0jFIWwQq9Fvp9In/aGMlSlo+ei1M2utzgJNB+fkI9oIqEOHN4bPuoa+JszyfbCe7oGnPIp5C4Vac1p8Xg32/8rtRmYFwRFRF+vxhjcawXG7fhV7NL3EfaW+D+OXdSwNzhJAwrjNa3SuNCxMW/0n9XKgwCHTirORtRs=",
			},
			http.StatusOK,
			"{\"sessionToken\":\"mock-session-token\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(sessionprocess.NewSessionProcessUseCase(logger, test.fields.sessionRegisterRepository))
			w := httptest.NewRecorder()
			requestBody := struct {
				PublicKeyPEM    string `json:"publicKeyPem"`
				SignedChallenge string `json:"signedChallenge"`
			}{
				PublicKeyPEM:    test.args.publicKeyPEM,
				SignedChallenge: test.args.signedChallenge,
			}

			requestBodyAsJSON, _ := json.Marshal(requestBody)
			request := httptest.NewRequest("POST", "/sessions/completechallenge", bytes.NewReader(requestBodyAsJSON))
			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}

func generateSessionProcessRepository(t *testing.T) *mock.MockSessionRegisterRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockSessionRegisterRepository(ctrl)
	return repo
}
