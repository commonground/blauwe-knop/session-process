// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"sessionprocess"
	http_infra "sessionprocess/http"
)

func Test_CreateRouter_StartChallenge(t *testing.T) {
	logger := zap.NewNop()

	mockDate := time.Date(2020, 9, 1, 15, 51, 0, 0, time.UTC)

	type fields struct {
		sessionRegisterRepository sessionprocess.SessionRegisterRepository
	}
	type args struct {
		publicKeyPEM string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				sessionRegisterRepository: func() sessionprocess.SessionRegisterRepository {
					repo := generateSessionProcessRepository(t)
					repo.EXPECT().CreateChallenge(publicKeyFingerPrint, publicKeyPEM).AnyTimes().Return(&sessionprocess.Challenge{
						PublicKey: publicKeyPEM,
						Date:      mockDate,
						Random:    "mock-random",
					}, nil)

					return repo
				}(),
			},
			args{
				publicKeyPEM: publicKeyPEM,
			},
			http.StatusOK,
			"{\"date\":1598975460,\"random\":\"mock-random\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(sessionprocess.NewSessionProcessUseCase(logger, test.fields.sessionRegisterRepository))
			w := httptest.NewRecorder()
			requestBody := struct {
				PublicKeyPEM    string `json:"publicKeyPem"`
				SignedChallenge string `json:"signedChallenge"`
			}{
				PublicKeyPEM: test.args.publicKeyPEM,
			}
			requestBodyAsJSON, _ := json.Marshal(requestBody)
			request := httptest.NewRequest("POST", "/sessions/startchallenge", bytes.NewReader(requestBodyAsJSON))
			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
