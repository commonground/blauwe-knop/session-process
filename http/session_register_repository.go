// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sessionprocess"
	"time"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SessionRegisterRepositoryEndpoint struct {
	baseURL string
	apiKey  string
}

type createSessionModel struct {
	PublicKeyPem string `json:"publicKeyPem"`
}
type createSessionResponseModel struct {
	SessionToken sessionprocess.SessionToken `json:"sessionToken"`
}

type createChallengeRequestModel struct {
	PublicKeyFingerprint string `json:"publicKeyFingerprint"`
	PublicKeyPem         string `json:"publicKeyPem"`
}

type getChallengeRequestModel struct {
	PublicKeyFingerprint string `json:"publicKeyFingerprint"`
}

type challengeResponseModel struct {
	PublicKey string `json:"publicKey"`
	Random    string `json:"random"`
	Date      int64  `json:"date"`
}

func NewSessionRegisterRepository(baseURL string, apiKey string) *SessionRegisterRepositoryEndpoint {
	return &SessionRegisterRepositoryEndpoint{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *SessionRegisterRepositoryEndpoint) CreateSession(publicKeyPem string) (*sessionprocess.SessionToken, error) {
	requestBody := createSessionModel{publicKeyPem}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/sessions/", s.baseURL)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create session token request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to create session token: %v", err)
	}

	if resp.StatusCode != http.StatusOK { //StatusCreated {
		return nil, fmt.Errorf("unexpected status while creating session token: %d", resp.StatusCode)
	}

	createSessionResponse := &createSessionResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(createSessionResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("SessionToken: %s", createSessionResponse.SessionToken)

	return &createSessionResponse.SessionToken, nil
}

func (s *SessionRegisterRepositoryEndpoint) CreateChallenge(publicKeyFingerprint, publicKeyPem string) (*sessionprocess.Challenge, error) {
	requestBody := createChallengeRequestModel{publicKeyFingerprint, publicKeyPem}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/sessions/challenge", s.baseURL)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create challenge request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to create challenge: %v", err)
	}

	if resp.StatusCode != http.StatusOK { //StatusCreated {
		return nil, fmt.Errorf("unexpected status while creating challenge: %d", resp.StatusCode)
	}

	challengeResponse := &challengeResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(challengeResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("PublicKey: %s", challengeResponse.PublicKey)
	log.Printf("Random: %s", challengeResponse.Random)
	log.Printf("Date: %d", challengeResponse.Date)

	return &sessionprocess.Challenge{PublicKey: challengeResponse.PublicKey, Random: challengeResponse.Random, Date: time.Unix(challengeResponse.Date, 0)}, nil
}

func (s *SessionRegisterRepositoryEndpoint) GetChallenge(publicKeyFingerPrint string) (*sessionprocess.Challenge, error) {
	requestBody := getChallengeRequestModel{publicKeyFingerPrint}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/sessions/challenge", s.baseURL)
	req, err := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to get challenge request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to get challenge: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving challenge: %d", resp.StatusCode)
	}

	challengeResponse := &challengeResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(challengeResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("PublicKey: %s", challengeResponse.PublicKey)
	log.Printf("Random: %s", challengeResponse.Random)
	log.Printf("Date: %d", challengeResponse.Date)

	return &sessionprocess.Challenge{PublicKey: challengeResponse.PublicKey, Random: challengeResponse.Random, Date: time.Unix(challengeResponse.Date, 0)}, nil
}

func (s *SessionRegisterRepositoryEndpoint) GetHealthCheck() healthcheck.Result {
	name := "session-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
