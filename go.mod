module sessionprocess

go 1.19

require (
	github.com/coreos/rkt v1.30.0
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.2
	github.com/golang/mock v1.6.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/stretchr/testify v1.8.1
	gitlab.com/commonground/blauwe-knop/health-checker v0.0.2
	go.uber.org/zap v1.24.0
	golang.org/x/crypto v0.3.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
