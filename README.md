# Session Process

Session Process is written in Golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

1. Download the required Go dependencies:

```sh
go mod download
```

2. Spin up Redis. We've provided instructions on how to do this using Docker Compose below.

3. Now start the session process:

```sh
go run cmd/session-process/main.go
```

Or run the session process using modd, which wil restart the API on file changes.

```sh
modd
```

By default, the session process will run on port `8087`.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

**Regenerating mocks**

```sh
sh regenerate-gomock-files.sh
```

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `azure-common-prod`

You will need to have a [Redis](https://redis.io/) database running on the cluster.
If you do not have Redis running on the cluster you can use `helm` to install it.

First add the chart to `helm`

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Now use `helm` to deploy the Redis chart on the cluster

```
helm install -n bk-test redis bitnami/redis
```

Once Redis is running you can use `helm` to deploy the session process

```
helm upgrade --install session-process ./charts/session-process -n bk-test
```
