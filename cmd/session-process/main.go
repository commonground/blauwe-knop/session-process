// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"
	"sessionprocess"

	"go.uber.org/zap"

	http_infra "sessionprocess/http"
)

type options struct {
	ListenAddress          string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8087" description:"Address for the session-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SessionRegisterAddress string `long:"session-register-address" env:"SESSION_REGISTER_ADDRESS" default:"http://localhost:8084" description:"Session register address."`
	SessionRegisterAPIKey  string `long:"session-register-api-key" env:"SESSION_REGISTER_API_KEY" default:"" description:"API key to use when calling the session-register service."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	sessionRegisterRepository := http_infra.NewSessionRegisterRepository(cliOptions.SessionRegisterAddress, cliOptions.SessionRegisterAPIKey)

	sessionProcessUseCase := sessionprocess.NewSessionProcessUseCase(logger, sessionRegisterRepository)

	router := http_infra.NewRouter(sessionProcessUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
